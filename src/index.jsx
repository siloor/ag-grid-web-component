import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

class AgGridWebComponent extends HTMLElement {
  connector = {};

  connectedCallback() {
    ReactDOM.render(<App connector={this.connector} />, this);
  }
}

customElements.define('ag-grid-web-component', AgGridWebComponent);
