import React from 'react';
import { AgGridReact } from 'ag-grid-react';

class App extends React.Component {
  constructor(props) {
    super(props);

    props.connector.setGridOptions = this.setGridOptions.bind(this);

    this.state = { gridOptions: {} };
  }

  setGridOptions(gridOptions) {
    this.setState({ gridOptions: gridOptions });
  }

  render() {
    return (
      <div className="ag-theme-alpine" style={{ height: 400, width: 600 }}>
        <AgGridReact {...this.state.gridOptions} />
      </div>
    );
  }
}

export default App;
